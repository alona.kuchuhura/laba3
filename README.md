## Лабораторна робота №3
___

**Тема:** "Дослідження методів багатопоточності та засобів паралельного API Java при розробки програмних компонентів розподіленної системі. Робота з пакетом java.util.concurrent"

## Хід роботи

### Завдання 1
## 1) Постановка завдання
![1.png](images%2F1.png)
![2.png](images%2F2.png)
![3.png](images%2F3.png)

## 2) Реалізація
**Клас Main**
![4.png](images%2F4.png)
---

**Клас MutexProgram**
![5.png](images%2F5.png)
![6.png](images%2F6.png)
![7.png](images%2F7.png)
---

**Клас SemaphoreProgram**
![8.png](images%2F8.png)
![9.png](images%2F9.png)
![10.png](images%2F10.png)
---

**Клас BlockingVariableProgram**
![11.png](images%2F11.png)
![12.png](images%2F12.png)
---

**Клас ThreadPoolSimulation**
![13.png](images%2F13.png)
![14.png](images%2F14.png)
---

## 3) Результати виконання
![15.png](images%2F15.png)
![16.png](images%2F16.png)
![17.png](images%2F17.png)
![18.png](images%2F18.png)
---


### Завдання 2  

#### Варіант № 14

## 1) Постановка завдання
![19.png](images%2F19.png)
![20.png](images%2F20.png)
---
## 2) Реалізація  
**Клас Main**
![21.png](images%2F21.png)

**Клас Storage**
![22.png](images%2F22.png)

**Клас Client**
![23.png](images%2F23.png)

**Клас Hairdresser**
![24.png](images%2F24.png)
![25.png](images%2F25.png)
![26.png](images%2F26.png)
---
## 3) Результати виконання
**Результат №1**
![27.png](images%2F27.png)

**Результат №2**
![28.png](images%2F28.png)

**Результат №3**
![29.png](images%2F29.png)
---