package kuchuhuraalona;
class Client implements Runnable {
    private final Hairdresser hairdresser;
    private final String name;

    public Client(Hairdresser hairdresser, String name) {
        this.hairdresser = hairdresser;
        this.name = name;
    }
    @Override
    public void run() {
        // спроба зайти в перукарню
        boolean entered = hairdresser.enterShop(name);
        // якщо впустили - підстригають
        if (entered) {
            try {
                hairdresser.getHaircut(name);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}