package kuchuhuraalona;

import java.util.concurrent.Semaphore;

class Hairdresser {
    private boolean isWorking;
    // Semaphore для контролю доступу до стільців
    private final Semaphore chairSemaphore = new Semaphore(4);
    private final Storage<String> clients = new Storage<>(4);

    // спить допоки клієнт його не розбудить
    public void goSleep() {
        setWorking(false);
        System.out.println("Hairdresser goes to sleep.");
        while (!isWorking() || clients.isEmpty()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }
    // запуск клієнтів в перукарню
    public boolean enterShop(String clientName) {
        if (chairSemaphore.tryAcquire()) {
            clients.add(clientName);
            System.out.println(clientName + " enters the shop.");
            if (!isWorking()) {
                System.out.println("Hairdresser wakes up.");
                setWorking(true);
            }
            return true;
        } else {
            System.out.println(clientName + " leaves because all chairs are occupied.");
            return false;
        }
    }

    public void getHaircut(String clientName) throws InterruptedException {
        while (clients.isEmpty() || !clients.peek().equals(clientName)) {
            Thread.sleep(100); // очікування поки дійде черга до цього клієнта
        }
        System.out.println("Hairdresser starts haircut for " + clientName);
        Thread.sleep(2000); // імітація стрижки
        System.out.println("Hairdresser finishes haircut for " + clientName);
        clients.poll(); // видалення клієнта з черги
        chairSemaphore.release(); // Release для стільця
    }
    public boolean isWorking() {
        return isWorking;
    }

    public void setWorking(boolean working) {
        isWorking = working;
    }


}
